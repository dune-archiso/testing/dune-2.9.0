#!/usr/bin/env bash

clear
echo ${BASH_VERSION}

function upgrade() {
  sudo pacman -Sy --needed --noconfirm
  pull
  clear
}

function pull() {
  sudo pacman --needed --noconfirm -S git >/dev/null 2>&1
  git pull origin main
  sudo pacman -Rscn --noconfirm git >/dev/null 2>&1
}

function clean() {
  rm -rf /tmp/makepkg
}

function make_dune-grid() {
  local PACKAGE=dune-grid
  pushd ${PACKAGE}
  echo "Hello from ${PWD}!"
  gpg --import keys/pgp/*.asc
  rm -f ${PACKAGE}-*-x86_64.pkg.tar.zst
  upgrade
  sudo pacman -Rscn --noconfirm ${PACKAGE} >/dev/null 2>&1
  makepkg --noconfirm -src | tee -a ${GITPOD_REPO_ROOT}/log_${PACKAGE}_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').log >/dev/null
  sudo pacman --noconfirm -U ${PACKAGE}-*-x86_64.pkg.tar.zst
  tar -I zstd -xvf ${PACKAGE}-*-x86_64.pkg.tar.zst | tee -a log_python-${PACKAGE}*.log
  pacman -Ql ${PACKAGE} >>log_python-${PACKAGE}*.log
  popd
  clean
}

function make_dune-geometry() {
  local PACKAGE=dune-geometry
  pushd ${PACKAGE}
  echo "Hello from ${PWD}!"
  gpg --import keys/pgp/*.asc
  rm -f ${PACKAGE}-*-x86_64.pkg.tar.zst
  upgrade
  sudo pacman -Rscn --noconfirm ${PACKAGE} >/dev/null 2>&1
  makepkg --noconfirm -src | tee -a ${GITPOD_REPO_ROOT}/log_${PACKAGE}_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').log >/dev/null
  sudo pacman --noconfirm -U ${PACKAGE}-*-x86_64.pkg.tar.zst
  tar -I zstd -xvf ${PACKAGE}-*-x86_64.pkg.tar.zst | tee -a log_python-${PACKAGE}*.log
  pacman -Ql ${PACKAGE} >>log_python-${PACKAGE}*.log
  popd
  clean
}

function make_dune-common() {
  local PACKAGE=dune-common
  pushd ${PACKAGE}
  echo "Hello from ${PWD}!"
  gpg --import keys/pgp/*.asc
  rm -f ${PACKAGE}-*-x86_64.pkg.tar.zst
  upgrade
  sudo pacman -Rscn --noconfirm ${PACKAGE} >/dev/null 2>&1
  makepkg --noconfirm -src | tee -a ${GITPOD_REPO_ROOT}/log_${PACKAGE}_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').log >/dev/null
  sudo pacman --noconfirm -U ${PACKAGE}-*-x86_64.pkg.tar.zst
  tar -I zstd -xvf ${PACKAGE}-*-x86_64.pkg.tar.zst | tee -a log_python-${PACKAGE}*.log
  pacman -Ql ${PACKAGE} >>log_python-${PACKAGE}*.log
  popd
  clean
}

[ ! -f ${GITPOD_REPO_ROOT}/dune-common/*.pkg.tar.zst ] || make_dune-common
[ ! -f ${GITPOD_REPO_ROOT}/dune-geometry/*.pkg.tar.zst ] || make_dune-geometry
[ ! -f ${GITPOD_REPO_ROOT}/dune-grid/*.pkg.tar.zst ] || make_dune-grid
[ ! -f ${GITPOD_REPO_ROOT}/dune-istl/*.pkg.tar.zst ] || make_dune-istl

cd ${GITPOD_REPO_ROOT}
