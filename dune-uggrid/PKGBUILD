pkgname=dune-uggrid
_tarver=2.9
_tar="releases/${_tarver}/${pkgname}-releases-${_tarver}.tar.gz"
pkgver=${_tarver}.0
pkgrel=1
pkgdesc="UG grid manager"
arch=('x86_64')
url="https://dune-project.org/modules/${pkgname}"
license=('custom:GPL2 with runtime exception')
depends=("dune-common>=${pkgver}")
makedepends=(doxygen graphviz)
checkdepends=(openssh)
optdepends=('doxygen: Generate the class documentation from C++ sources'
  'graphviz: Graph visualization software')
source=(https://gitlab.dune-project.org/staging/${pkgname}/-/archive/${_tar})
sha512sums=('86d76b968d878b3c8e926230f7e3bf6b31429cc17a2eec2e870fb972779d9fd0f7a5bf216540cb9c118c078333991f3cf3f149f8e585a99c57cb2c8771455c8e')

build() {
  cmake \
    -S ${pkgname}-releases-${_tarver} \
    -B build-cmake \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=TRUE \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
    -DCMAKE_CXX_FLAGS="-Wall -fdiagnostics-color=always -mavx" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -DENABLE_HEADERCHECK=ON \
    -Wno-dev
  cmake --build build-cmake --target all
}

check() {
  cmake --build build-cmake --target build_tests
  ctest --verbose --output-on-failure --test-dir build-cmake
}

package() {
  DESTDIR="${pkgdir}" cmake --build build-cmake --target install
  install -Dm644 ${pkgname}-releases-${_tarver}/COPYING "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  find "${pkgdir}" -type d -empty -delete
}
